package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.enumerated.Sort;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M model : records) {
            if (userId.equals(model.getUserId())) result.add(model);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Override
    public int getSize(final String userId) {
        int count = 0;
        for (final M model : records) {
            if (userId.equals(model.getUserId())) count++;
        }
        return count;
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M model : records) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId,id) != null;
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        models.clear();
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

}
