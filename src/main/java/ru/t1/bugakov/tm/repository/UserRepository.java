package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.exception.entity.UserNotFoundException;
import ru.t1.bugakov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public Boolean isLoginExist(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}