package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.enumerated.Sort;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    int getSize(String userId);

    M findById(String userId, String id);

    M findByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    void clear(String userId);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

}
