package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractUserOwnedService<Task> {

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

}
