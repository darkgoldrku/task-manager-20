package ru.t1.bugakov.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

}
