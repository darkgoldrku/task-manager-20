package ru.t1.bugakov.tm.api.comparator;

public interface IHasName {

    String getName();

    void setName(final String name);

}
