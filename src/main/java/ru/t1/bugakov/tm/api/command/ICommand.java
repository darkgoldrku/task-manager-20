package ru.t1.bugakov.tm.api.command;

public interface ICommand {

    void execute();

    String getName();

    String getArgument();

    String getDescription();

}
