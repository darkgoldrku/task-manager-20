package ru.t1.bugakov.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Index is incorrect.");
    }

    public NumberIncorrectException(final String value, final Throwable cause) {
        super("Error! This value \"" + value + "\" is incorrect.");
    }

}
