package ru.t1.bugakov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        final String userId = getAuthService().getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
