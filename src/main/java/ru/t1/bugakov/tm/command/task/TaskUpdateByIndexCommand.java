package ru.t1.bugakov.tm.command.task;

import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }

    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public String getDescription() {
        return "Update task by index.";
    }

}
