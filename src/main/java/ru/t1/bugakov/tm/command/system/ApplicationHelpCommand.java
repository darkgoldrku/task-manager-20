package ru.t1.bugakov.tm.command.system;

import ru.t1.bugakov.tm.api.command.ICommand;
import ru.t1.bugakov.tm.api.service.ICommandService;
import ru.t1.bugakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final ICommandService commandService = getCommandService();
        final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Show list arguments.";
    }

}
