package ru.t1.bugakov.tm.command.task;

import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        final Task task = getTaskService().findById(userId, id);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Show task by id.";
    }

}
